# Shiny Dashboard for A-16-037 Partial Occlusion REBOA

The script in the `metrotransit-data/` directory will fetch schedule data for the Twin Cities Metro Transit. This data is updated weekly.


Data format reference: https://developers.google.com/transit/gtfs/reference?csw=1
